#include <iostream>
#include <memory>

#include "Session.h"

int main(int argc, char** argv)
{
	const auto host = "127.0.0.1";
	const auto port = "2751";

	// The io_context is required for all I/O
	boost::asio::io_context ioContext;

	RequestsManager requestsManager;

	auto averageHP = requestsManager.getMap().at("AverageHP")->getContentAsString();
	auto defense = requestsManager.getMap().at("Defense")->getContentAsString();
	auto sumTotal = requestsManager.getMap().at("SumTotal")->getContentAsString();
	auto topSpeed = requestsManager.getMap().at("TopSpeed")->getContentAsString();
	auto topTotal = requestsManager.getMap().at("TopTotal")->getContentAsString();

	// Launch the asynchronous operation
	std::make_shared<Session>(ioContext)->run(host, port, averageHP);
	std::make_shared<Session>(ioContext)->run(host, port, defense);
	std::make_shared<Session>(ioContext)->run(host, port, sumTotal);
	std::make_shared<Session>(ioContext)->run(host, port, topSpeed);
	std::make_shared<Session>(ioContext)->run(host, port, topTotal);

	// Run the I/O service. The call will return when
	// the socket is closed.
	ioContext.run();

	system("pause");
	return EXIT_SUCCESS;
}