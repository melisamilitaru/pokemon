#include "RequestsManager.h"

#include "AverageHPRequest.h"
#include "DefenseRequest.h"
#include "SumTotalRequest.h"
#include "TopSpeedRequest.h"
#include "TopTotalRequest.h"

RequestsManager::RequestsManager()
{
	this->requests.emplace("AverageHP", std::make_shared<AverageHPRequest>());
	this->requests.emplace("Defense", std::make_shared<DefenseRequest>());
	this->requests.emplace("SumTotal", std::make_shared<SumTotalRequest>());
	this->requests.emplace("TopSpeed", std::make_shared<TopSpeedRequest>());
	this->requests.emplace("TopTotal", std::make_shared<TopTotalRequest>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
