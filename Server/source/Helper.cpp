#include "Helper.h"
#include <boost\/filesystem.hpp>
#include <boost/algorithm/string.hpp>

using namespace boost::filesystem;

std::vector<Pokemon> Helper::read() const
{
	ifstream file("C:/Users/admin/Desktop/Pokemon/Server/Pokemon.txt");
	std::string data;
	std::vector<Pokemon> pokemons;
	std::string firstLine;
	if (getline(file, data))
		firstLine = data;
	//std::cout << "First line is: " << firstLine;
	//std::cout << std::endl;

	while (getline(file, data))
	{
		std::vector<std::string> tokens;
		boost::split(tokens, data, boost::is_any_of(","));
		Pokemon pokemon = Pokemon(std::stoi(tokens[0]), tokens[1], tokens[2], tokens[3], std::stoi(tokens[4]), std::stoi(tokens[5]),
			std::stoi(tokens[6]), std::stoi(tokens[7]), std::stoi(tokens[8]), std::stoi(tokens[9]), std::stoi(tokens[10]));
		pokemons.push_back(pokemon);
	}

	/*for each (auto pokemon in pokemons)
		std::cout << "Id:" << pokemon.id << " Name:" << pokemon.name << " Type1:" << pokemon.type1 << " Type2:" <<
		pokemon.type2 << " Total:" << pokemon.total << " HP:" << pokemon.hp << " Attack:" << pokemon.attack << " Defense:" <<
		pokemon.defense << " Sp. Atk:" << pokemon.spAtk << " Sp. Def:" << pokemon.spDef << " Speed:" << pokemon.speed << std::endl;*/

	file.close();
	return pokemons;
}