#include "Pokemon.h"

Pokemon::Pokemon(int id, std::string name, std::string type1, std::string type2, int total, int hp, int attack, int defense, int spAtk, int spDef, int speed)
{
	this->id = id;
	this->name = name;
	this->type1 = type1;
	this->type2 = type2;
	this->total = total;
	this->hp = hp;
	this->attack = attack;
	this->defense = defense;
	this->spAtk = spAtk;
	this->spDef = spDef;
	this->speed = speed;
}
