#include "ResponsesManager.h"

#include "AverageHPResponse.h"
#include "DefenseResponse.h"
#include "SumTotalResponse.h"
#include "TopSpeedResponse.h"
#include "TopTotalResponse.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("AverageHP", std::make_shared<AverageHPResponse>());
	this->Responses.emplace("Defense", std::make_shared<DefenseResponse>());
	this->Responses.emplace("SumTotal", std::make_shared<SumTotalResponse>());
	this->Responses.emplace("TopSpeed", std::make_shared<TopSpeedResponse>());
	this->Responses.emplace("TopTotal", std::make_shared<TopTotalResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
