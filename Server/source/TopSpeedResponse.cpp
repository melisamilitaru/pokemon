#include "TopSpeedResponse.h"
#include <boost/sort/spreadsort/spreadsort.hpp>

TopSpeedResponse::TopSpeedResponse() : Response("TopSpeed")
{

}

std::string TopSpeedResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	Helper helper;
	std::vector<Pokemon> pokemons = helper.read();
	std::vector<std::pair<int, std::string>> nameSpeed;
	std::vector<int> speed;
	std::vector<std::string> names;
	for each(auto pokemon in pokemons)
	{
		speed.push_back(pokemon.speed);
		names.push_back(pokemon.name);
	}
	for (int index = 0; index < speed.size(); index++)
	{
		nameSpeed.push_back(std::make_pair(speed[index], names[index]));
	}
	std::sort(nameSpeed.begin(), nameSpeed.end(), [](auto &left, auto &right) {
		return left.first > right.first;
	});
	this->content.push_back(boost::property_tree::ptree::value_type("File", "topSpeed.txt"));
	for (int index = 0; index < 10; index++)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(nameSpeed[index].second, std::string(std::to_string(nameSpeed[index].first))));
	}

	return this->getContentAsString();
}