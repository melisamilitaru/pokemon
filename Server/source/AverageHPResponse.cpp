#include "AverageHPResponse.h"

#include <boost/range/numeric.hpp>

AverageHPResponse::AverageHPResponse() : Response("AverageHP")
{

}

std::string AverageHPResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	Helper helper;
	std::vector<Pokemon> pokemons = helper.read();
	std::vector<int> vectorHP;
	for each(auto pokemon in pokemons)
	{
		vectorHP.push_back(pokemon.hp);
	}
	float sum = boost::accumulate(vectorHP, 0);
	this->content.push_back(boost::property_tree::ptree::value_type("File", "averageHP.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Sum", std::to_string(sum)));
	this->content.push_back(boost::property_tree::ptree::value_type("Number of Pokemons", std::to_string(vectorHP.size())));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", std::to_string(sum / vectorHP.size())));

	return this->getContentAsString();
}
