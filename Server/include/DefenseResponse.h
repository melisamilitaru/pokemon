#pragma once

#include "Response.h"
#include "Helper.h"

class DefenseResponse : public Framework::Response
{
public:
	DefenseResponse();
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};