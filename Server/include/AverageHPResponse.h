#pragma once

#include "Response.h"
#include "Helper.h"

class AverageHPResponse : public Framework::Response
{
public:
	AverageHPResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};