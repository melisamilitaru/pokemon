#pragma once
#include "Pokemon.h"
#include <vector>

class Helper
{
public:
	Helper() = default;
	std::vector<Pokemon> read() const;
};